import boto3
import os
import requests
import pandas as pd
from akumen_api import get_results_views, get_results, AKUMEN_API_URL, API_KEY


def get_model(model_name):
    url = f'{AKUMEN_API_URL}models/{model_name}'

    response = requests.get(url, headers={'Authorization': API_KEY, 'Content-Type': 'application/json'})
    response.raise_for_status()

    return response.json()


def akumen(**kwargs):
    """
    Parameters:
        - Input: source [scenario]
        - Input: views [list]
        - Input: scope [string]
        - Input: flatten [boolean]
        - Input: bucket_name [string]
        - Input: bucket_path [string]
        - Input: access_key [string]
        - Input: secret_access_key [string]

        - Output: data_size [float]
    """
    
    if not kwargs.get('access_key') or not kwargs.get('secret_access_key'):
        raise Exception('Did not specify access_key or secret_access_key.')
    
    if not kwargs.get('bucket_name'):
        raise Exception('Did not specify bucket name.')

    if kwargs.get('scope').lower() not in ['model', 'study', 'scenario']:
        raise Exception('Scope must be specified and one of [model, study, scenario].')

    flatten = kwargs.get('flatten', False)
    bucket_path = kwargs.get('bucket_path', '')

    session = boto3.Session(
        aws_access_key_id=kwargs.get('access_key'),
        aws_secret_access_key=kwargs.get('secret_access_key')
    )

    os.makedirs('data', exist_ok=True)

    s3 = session.client('s3')

    source = kwargs.get('source')
    scope = kwargs.get('scope').lower()
    views = kwargs.get('views')
    if not views:
        # if no mapping was specified, do all views with filenames equalling the views
        views = get_results_views(source['model_name'])

    files = []
    for view in views:
        if scope == 'scenario':
            if flatten:
                filepath = os.path.join(view + '.csv')
            else:
                filepath = os.path.join(source["model_name"], source["study_name"], source["scenario_name"], view + '.csv')
                os.makedirs(os.path.join('data', os.path.dirname(filepath)), exist_ok=True)
            df = get_results(source['model_name'], source['study_name'], source['scenario_name'], view)
            df.to_csv(os.path.join('data', filepath), index=False)
            files.append(filepath)
        elif scope == 'study':
            # we need to get the names of the scenarios in this study
            dfs = []
            model = get_model(source['model_name'])
            for study in model['studies']:
                if study['name'] == source['study_name']:
                    for scenario in study['scenarios']:
                        df = get_results(source['model_name'], source['study_name'], scenario['name'], view)
                        if flatten:
                            dfs.append(df)
                        else:
                            filepath = os.path.join(source["model_name"], source["study_name"],
                                                    scenario['name'], view + '.csv')
                            os.makedirs(os.path.join('data', os.path.dirname(filepath)), exist_ok=True)
                            df.to_csv(os.path.join('data', filepath), index=False)
                            files.append(filepath)
            if flatten:
                df = pd.concat(dfs)
                filepath = os.path.join(view + '.csv')
                df.to_csv(os.path.join('data', filepath), index=False)
                files.append(filepath)
        elif scope == 'model':
            # we need to get the names of the scenarios in every study
            dfs = []
            model = get_model(source['model_name'])
            for study in model['studies']:
                for scenario in study['scenarios']:
                    df = get_results(source['model_name'], study['name'], scenario['name'], view)
                    if flatten:
                        dfs.append(df)
                    else:
                        filepath = os.path.join(source["model_name"], study['name'], scenario['name'],
                                                view + '.csv')
                        os.makedirs(os.path.join('data', os.path.dirname(filepath)), exist_ok=True)
                        df.to_csv(os.path.join('data', filepath), index=False)
                        files.append(filepath)
            if flatten:
                df = pd.concat(dfs)
                filepath = os.path.join(view + '.csv')
                df.to_csv(os.path.join('data', filepath), index=False)
                files.append(filepath)

    # now let's upload them
    total_size = 0
    for filepath in files:
        total_size += os.path.getsize(os.path.join('data', filepath))
        s3.upload_file(os.path.join('data', filepath), kwargs.get('bucket_name'), filepath)

    return {
        'data_size': total_size
    }
